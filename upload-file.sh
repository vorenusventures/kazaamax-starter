#!/bin/bash

source ~/.mobilelocker
# This is the ID of the presentation in Mobile Locker.  Look at your browser's URL to retrieve this value.
PRESENTATION_ID=[The presentation's ID from the admin portal]

if [ "$#" -ne 2 ]
then
  echo "Usage: ./upload-file.sh LOCAL_FILE UPLOAD_PATH"
  echo "LOCAL_PATH is the location of the file on your computer.  EX: dist/index.html
  echo "UPLOAD_PATH is the path to the file, relative to the presentation's base directory in Mobile Locker.  It should not start with a slash.  EX: index.html, js/data.json
  exit 1
fi

LOCAL_FILE=$1
UPLOAD_PATH=$2

# Use CURL to upload the file file.
AUTH_HEADER="Authorization: Bearer $API_TOKEN"
UPLOAD_URL="https://mobilelocker.com/api/presentations/$PRESENTATION_ID/files/upload"
curl \
--header "$AUTH_HEADER" \
--form path="$UPLOAD_PATH" \
--form file=@$LOCAL_FILE \
"$UPLOAD_URL"

# You will receive an email notification (and SMS if you added your mobile number) when it's ready.
# Open Mobile Locker and pull-to-refresh to get the latest changes
