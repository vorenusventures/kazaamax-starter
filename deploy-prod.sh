#!/bin/bash

source $HOME/.mobilelocker
source ./site_config
API_URL="https://mobilelocker.com/api/presentations/$PRESENTATION_ID/upload"
clear
echo "PRESENTATION_ID: $PRESENTATION_ID"
echo "API_URL:         $API_URL"
read -p "Press ENTER to continue."

gulp --production

./package-presentation.sh

read -p "ZIP Complete. Press ENTER to upload."

# Scan the presentation to Mobile Locker using your API TOKEN
AUTH_HEADER="Authorization: Bearer $ML_API_TOKEN"
curl \
    --header "$AUTH_HEADER" \
    --form file=@presentation.zip \
    "$API_URL"

echo ""
echo "COMPLETE"
