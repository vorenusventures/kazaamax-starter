This is a starter project for building Mobile Locker presentations.
 
Read more about Mobile Locker here:
* [Mobile Locker Website](https://mobilelocker.com)
* [Mobile Locker Developer Docs](https://mobilelocker.readme.io/)


## Prerequisites:
* Install [node.js](https://nodejs.org/en/)
* Install [gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
* Install [yarn](https://yarnpkg.com/)

Then:
```bash
yarn install
```

## Building

```bash
gulp && gulp watch
```

Then go to [http://localhost:3000](http://localhost:3000)

This presentation is built with [VueJS](https://vuejs.org).

Read the source for [app.js](src/master/resources/assets/js/app.js) and [home.vue](src/master/resources/assets/js/components/home.vue) to get a sense of how it works.

