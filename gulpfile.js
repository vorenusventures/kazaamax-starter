var elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

let config = elixir.config;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
  mix.copy('node_modules/bootstrap-sass/assets/fonts/**', 'public/fonts/');
  mix.copy('node_modules/font-awesome/fonts/**', 'public/fonts/');
  mix.copy('node_modules/video.js/dist/font/**', 'public/fonts/');
  mix.copy('resources/assets/fonts/**', 'public/fonts/');
  mix.copy('resources/assets/images/**', 'public/images/');
  mix.copy('resources/assets/pdfs/**', 'public/pdfs/');
  mix.copy('resources/assets/videos/**', 'public/videos/');
  mix.copy('resources/views/index.html', './public/index.html');
  mix.copy('resources/assets/js/forms.json', 'public/');
  mix.styles([
    './node_modules/toastr/build/toastr.css',
  ], './public/css/vendors.css');
  mix.webpack('app.js');
  mix.sass('app.scss');
  mix.browserSync({
    server: ['public'],
    proxy: null,
    notify: false,
    open: false
  });
});
