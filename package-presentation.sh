#!/bin/bash

# zip everything into "presentation.zip"
rm -f presentation.zip
cd public
zip -r --exclude=.gitignore ../presentation.zip .
cd ../
