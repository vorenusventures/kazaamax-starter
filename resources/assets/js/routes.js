module.exports = [
  {
    name: 'home',
    path: '',
    component: require('./components/home.vue'),
    meta: {
      title: 'Home'
    }
  },
  {
    name: 'demo-form',
    path: '/demo-form',
    component: require('./components/demo-form.vue'),
    meta: {
      title: 'Demo Form'
    }
  },
  {
    path: '*',
    redirect: '/home'
  }
];
