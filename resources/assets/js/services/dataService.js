import _ from "lodash";
const pages = [
  {
    id: 'home',
    name: 'Home',
    title: 'Home',
    items: []
  }
];

export default {
  getPages() {
    return pages
  },
  getPage(pageID) {
    return _.find(pages, page => {
      return page.id == pageID;
    })
  }
}
