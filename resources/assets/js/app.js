window.toastr = require('toastr');
toastr.options.closeButton = true;
toastr.options.positionClass = 'toast-top-full-width';
toastr.options.preventDuplicates = true;
toastr.options.progressBar = true;
toastr.options.timeOut = 3000; // milliseconds

window.lodash = window._ = require('lodash');
window.jQuery = window.$ = require('jquery');
window.moment = require('moment');

require('bootstrap-sass');

import Vue from "vue";
window.Vue = Vue;
import VueRouter from "vue-router";
import VueResource from "vue-resource";
import Validation from 'vuelidate'; // https://github.com/monterail/vuelidate
import routes from "./routes";
require('./forms/SparkErrors');
require('./forms/SparkForm');
require('./forms/components');
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Validation);

import f from './filters/filters';
f.loadVueFilters();
Vue.component('app', require('./components/app.vue'));
Vue.component('modal', require('./components/modal.vue'));
Vue.component('navbar', require('./components/navbar.vue'));

const router = new VueRouter({
  linkActiveClass: 'active',
  routes
});
router.beforeEach((to, from, next) => {
  $('#nav').collapse('hide');
  next();
});

const bus = new Vue();
window.bus = bus;

const app = new Vue({
  el: '#app',
  router
});
