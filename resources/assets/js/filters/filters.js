import Vue from 'vue';
import moment from 'moment';

export default {
  loadVueFilters() {
    if (typeof window.Vue == 'undefined') {
      console.error(`The Vue object is not defined`);
      window.Vue = Vue;
      return;
    }
    if (typeof window.moment =='undefined') {
      console.error(`The moment object is not defined`);
      window.moment = moment;
      return;
    }

    Vue.filter('date', (value, format = 'YYYY-MM-DD') => {
      return moment.utc(value).local().format(format)
    });


    /**
     * Format the given date as a timestamp.
     */
    Vue.filter('datetime', (value, format = 'YYYY-MM-DD HH:mm:ss') => {
      return moment.utc(value).local().format(format);
    });


    /**
     * Format the given date into a relative time.
     */
    Vue.filter('relative', value => {
      moment.updateLocale('en', {
        relativeTime : {
          future: "in %s",
          past:   "%s",
          s:  "1s",
          m:  "1m",
          mm: "%dm",
          h:  "1h",
          hh: "%dh",
          d:  "1d",
          dd: "%d days",
          M:  "1 month ago",
          MM: "%d months ago",
          y:  "1y",
          yy: "%dy"
        }
      });

      return moment.utc(value).local().fromNow();
    });
  }
}
