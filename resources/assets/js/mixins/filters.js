import _ from 'lodash';

const orderBy = function(items, field) {
  return _.orderBy(items, [item => item[field].toString().trim().toLowerCase()]);
};

const capitalize = require('capitalize');

const cap = (string) => {
  if (string == null) {
    return null;
  }
  return capitalize(string);
};

const capWords = (string) => {
  if (string == null) {
    return null;
  }
  return capitalize.words(string);
}

module.exports = {
  methods: {
    pluralize: require('pluralize'),
    accounting: require('accounting'),
    capitalize: cap,
    capitalizeWords: capWords,
    orderBy: orderBy,
  }
}
